# Créer un environnement WorPress sous Docker

[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/benoitmazurier/)

Durée estimée : **30 minutes**

## Description

Ce tutoriel vous guidera dans la création d'un environnement Docker pour exécuter WordPress en local. Vous découvrirez comment configurer un conteneur Docker pour héberger la base de données de WordPress et comment installer une image Docker pour l'application WordPress elle-même. À la fin de ce tutoriel, vous aurez une installation fonctionnelle de WordPress en cours d'exécution sur votre ordinateur local, ce qui vous permettra de développer et de tester vos sites WordPress sans avoir à les déployer en ligne.

## Qu'est ce que Docker

Docker est un logiciel de virtualisation d'applications qui permet de créer, déployer et exécuter des applications dans des containers isolés. Les containers sont des espaces de travail virtuels qui exécutent une application et ses dépendances dans un environnement défini et isolé du système d'exploitation hôte. Chaque container a son propre système de fichiers, ses propres ressources réseau et sa propre configuration système, ce qui permet de garantir la portabilité, la stabilité et l'isolation des applications.

Le fonctionnement de Docker se base sur une architecture client-serveur. Le client Docker envoie des commandes au daemon Docker qui exécute les instructions et gère les containers. Les images Docker sont utilisées pour définir l'application et ses dépendances, et peuvent être téléchargées depuis un registre Docker public ou créées à partir de scripts Docker. Les containers peuvent être démarrés, arrêtés et supprimés à volonté, et plusieurs containers peuvent être exécutés sur un même ordinateur.

## Installation de Docker

### MacOs

Les étapes pour installer Docker sous MacOS sont les suivantes :

- Inscrivez-vous sur le site web de Docker : Allez sur le site web de [Docker](https://www.docker.com/) et créez un compte pour télécharger l'installateur Docker pour macOS.
  
- Téléchargez Docker Desktop : Une fois connecté, téléchargez l'installateur Docker Desktop pour macOS en cliquant sur le bouton de téléchargement. *(Attention, choisissez la version adapté au processeur de votre Mac)*

- Installez Docker Desktop : Double-cliquez sur le fichier téléchargé pour démarrer l'installation et suivez les instructions pour installer Docker sur votre Mac.

- Vérifiez l'installation de Docker : Une fois l'installation terminée, ouvrez une fenêtre Terminal et tapez la commande suivante qui doit vous retourner la version de Docker installé.

  ```bash
  docker -v
  ```

- Vérifiez l'installation de Docker Compose : Dans la fenêtre Terminal, tapez la commande suivante qui doit vous retourner la version de Docker Compose installé.

  ```bash
  docker-compose -v
  ```

- Configurez les paramètres : Pour configurer les paramètres de Docker Desktop, cliquez sur l'icône Docker dans la barre des tâches et sélectionnez Préférences. Ici, vous pouvez ajuster les paramètres de mémoire, de CPU et de stockage alloués à Docker.

### Windows 10/11

Les étapes pour installer Docker sous Windows 10/11 sont les suivantes :

- Inscrivez-vous sur le site web de Docker : Allez sur le site web de [Docker](https://www.docker.com/) et créez un compte pour télécharger l'installateur Docker pour Windows.

- Téléchargez Docker Desktop : Une fois connecté, téléchargez l'installateur Docker Desktop pour Windows en cliquant sur le bouton de téléchargement.

- Installez Docker Desktop : Double-cliquez sur le fichier téléchargé pour démarrer l'installation et suivez les instructions pour installer Docker sur votre ordinateur Windows.

- Vérifiez l'installation de Docker : Une fois l'installation terminée, ouvrez une fenêtre PowerShell et tapez la commande suivante qui doit vous retourner la version de Docker installé.

  ```bash
  docker -v
  ```

- Vérifiez l'installation de Docker Compose : Dans la fenêtre PowerShell, tapez la commande suivante qui doit vous retourner la version de Docker Compose installé.

  ```bash
  docker-compose -v
  ```

- Configurez les paramètres : Pour configurer les paramètres de Docker Desktop, cliquez sur l'icône Docker dans la barre des tâches et sélectionnez Paramètres. Ici, vous pouvez ajuster les paramètres de mémoire, de CPU et de stockage alloués à Docker.

### Ubuntu

Les étapes pour installer Docker sous Ubuntu sont les suivantes :

- Mettre à jour les packages système : Ouvrez une fenêtre Terminal et tapez les commandes suivantes pour mettre à jour les packages système sur votre Ubuntu.

  ```bash
  sudo apt update
  sudo apt upgrade
  ```

- Ajouter la clé GPG de Docker : Pour ajouter la clé GPG de Docker, tapez la commande suivante dans votre Terminal.

  ```bash
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  ```

- Ajouter le référentiel Docker : Pour ajouter le référentiel Docker à votre système, tapez la commande suivante dans votre Terminal:

  ```bash
  sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
  ```

- Mettre à jour les packages Docker : Pour mettre à jour les packages Docker, tapez les commandes suivantes dans votre Terminal.

  ```bash
  sudo apt update
  sudo apt install docker-ce docker-ce-cli containerd.io
  ```

- Vérifiez l'installation : Pour vérifier que Docker est installé et fonctionne correctement, tapez la commande suivante dans votre Terminal.

  ```bash
  sudo docker -v
  sudo docker-compose -v
  ```

## Téléchargement du fichier de configuration Docker

Pour la suite de ce tutoriel vous aurez besoin d'un fichier **docker-compose.yml**.

Ce fichier au format **YAML**, contient la configuration pour Docker Compose, qui définit les services, les réseaux et les volumes utilisés par une application Docker. Il contient les éléments suivants :

- Les services : Les services décrivent les images Docker et les configurations associées qui seront utilisées pour créer des conteneurs.

- Les réseaux : Les réseaux décrivent comment les services peuvent communiquer les uns avec les autres, notamment en définissant les réseaux internes qui seront créés pour l'application.

- Les volumes : Les volumes décrivent les données partagées entre les conteneurs et le système de fichiers de l'hôte.

- Les variables d'environnement : Les variables d'environnement peuvent être définies pour chaque service, ce qui permet de passer des paramètres à l'application.

- Les ports : Les ports décrivent comment les services peuvent être accessibles en dehors de l'application Docker, en publiant des ports sur le système hôte.

Téléchargez le fichier **docker-compose.yml** présent dans le dossier correspondant à la configuration de votre machine.

## Démarrage de Docker Desktop

Lancez l'application Docker Desktop sur votre machine et attendez que tous ses services soient opérationnels. Vous saurez que Docker est prêt quand le rectangle en bas à gauche de l'interface affichant le logo de Docker sera vert.

## Initialisation de votre application WordPress

Maintenant que vous avez téléchargé le fichier **docker-compose.yml**, créez un dossier sur votre ordinateur avec le nom de votre future application et déposez-y le fichier de configuration.

Une fois cette action réalisé, deux choix s'offres à vous :

- Option n°1 : Ouvrez un terminal de commande depuis le dossier contenant le fichier **docker-compose.yml**.

- Option n°2 : Ouvrez un terminal de commande depuis n'importe où et éxecutez la commande `cd` suivi du chemin du dossier content le fichier de configuration **docker-compose.yml**. *(Astuce : Faites un glisser-déposer du dossier concerné dans le terminal de commande pour obtenir son chemin)*

  ```bash
  cd CHEMIN_DU_DOSSIER
  ```

Exécutez la commande suivante dans le terminal de commande, une fois l'une des deux options ci-dessus éxecutée.

```bash
docker-compose up -d
```

## Vérification du déploiement de votre application

Rendez-vous sur l'onglet "Containers" de Docker Desktop, vous devriez voir le nom de votre application précédé d'une icône verte. Si tel est le cas, félicitations, vous êtes maintenant en possession d'une application WordPress sous Docker hébergée localement.

## Accès à l'application

Pour vous rendre :

- Sur votre application WordPress, rendez-vous sur l'url : [http://127.0.0.1:8000](http://127.0.0.1:8000).

- Sur l'interface PhpMyAdmin, rendez-vous sur l'url : [http://127.0.0.1:8080](http://127.0.0.1:8080).
  - Identifiant : `admin`
  - mot de passe : `root`

## Utilisation

Lorsque vous souhaitez accéder à votre application WordPress, suivez les étapes suivantes :

- Lancez Docker Desktop et attendez que ses services soient opérationnels.

- Si l'icône de votre application n'est pas verte, démarrez les conteneurs Docker.

- Votre application est accessible via l'url présente dans la partie [Accès à l'application](README.md#accès-à-lapplication).

Pour éteindre les services de Docker :

- Depuis la barre des tâches, clic droit sur le logo Docker puis "Quit Docker Desktop".
